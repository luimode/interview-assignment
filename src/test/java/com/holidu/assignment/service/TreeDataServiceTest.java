package com.holidu.assignment.service;

import com.holidu.assignment.api.OccurrenceAggregation;
import com.holidu.assignment.persistence.TreeDataQueries;
import com.holidu.assignment.persistence.TreeOccurrenceAggregation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TreeDataServiceTest {

    private static final double LONGITUDE = 50.0;
    private static final double LATITUDE = 60.0;
    private static final double RADIUS = 100.0;

    @Mock
    TreeDataQueries treeDataQueries;

    @InjectMocks
    TreeDataService treeDataService;

    @Test
    public void testGetTreeSpeciesWithinRadius_validQueryResult_returnsOccurrenceAggregation() {
        String species = "species";
        int count = 3;
        List<TreeOccurrenceAggregation> returnedOccurrenceAggregations = Collections.singletonList(
                new TreeOccurrenceAggregation(species, count));
        when(treeDataQueries.queryTreeSpeciesOccurrenceWithinRadius(anyDouble(), anyDouble(), anyDouble()))
                .thenReturn(returnedOccurrenceAggregations);
        OccurrenceAggregation expectedResult = new OccurrenceAggregation();
        expectedResult.put(species, count);

        OccurrenceAggregation treeSpeciesWithinRadius = treeDataService.getTreeSpeciesWithinRadius(LONGITUDE, LATITUDE, RADIUS);

        assertEquals(expectedResult, treeSpeciesWithinRadius);
    }

    @Test
    public void testGetTreeSpeciesWithinRadius_emptyQueryResult_returnsEmptyAggregation() {
        when(treeDataQueries.queryTreeSpeciesOccurrenceWithinRadius(anyDouble(), anyDouble(), anyDouble()))
                .thenReturn(Collections.emptyList());

        OccurrenceAggregation treeSpeciesWithinRadius = treeDataService.getTreeSpeciesWithinRadius(LONGITUDE, LATITUDE, RADIUS);

        assertNotNull(treeSpeciesWithinRadius);
        assertTrue(treeSpeciesWithinRadius.isEmpty());
    }

    @Test
    public void testGetTreeSpeciesWithinRadius_nullQueryResult_returnsEmptyAggregation() {
        when(treeDataQueries.queryTreeSpeciesOccurrenceWithinRadius(anyDouble(), anyDouble(), anyDouble()))
                .thenReturn(null);

        OccurrenceAggregation treeSpeciesWithinRadius = treeDataService.getTreeSpeciesWithinRadius(LONGITUDE, LATITUDE, RADIUS);

        assertNotNull(treeSpeciesWithinRadius);
        assertTrue(treeSpeciesWithinRadius.isEmpty());
    }
}
