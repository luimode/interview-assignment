package com.holidu.assignment.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.holidu.assignment.api.OccurrenceAggregation;
import com.holidu.assignment.service.TreeDataService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TreeDataRestController.class)
public class TreeDataRestControllerTestIT {

    private static final String API_PREFIX = "/api/v1";

    private final double VALID_LONGITUDE = 50;
    private final double VALID_LATITUDE = 60;
    private final double VALID_RADIUS = 100;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TreeDataService treeDataService;

    @Test
    public void testAggregatedSearchGet_validRequest_returnsOk() throws Exception {
        OccurrenceAggregation expectedOccurrenceAggregation = new OccurrenceAggregation();
        expectedOccurrenceAggregation.put("speciesName", 10);
        when(treeDataService.getTreeSpeciesWithinRadius(VALID_LONGITUDE, VALID_LATITUDE, VALID_RADIUS))
                .thenReturn(expectedOccurrenceAggregation);

        MvcResult result = getRequestResult(VALID_LONGITUDE, VALID_LATITUDE, VALID_RADIUS, status().isOk());

        assertEquals(objectMapper.writeValueAsString(expectedOccurrenceAggregation),
                result.getResponse().getContentAsString());
    }

    @Test
    public void testAggregatedSearchGet_invalidLongitude_returns400() throws Exception {
        String expectedError = objectMapper.writeValueAsString(Collections.singletonMap("error",
                "aggregatedSearchGet.longitude: must be greater than or equal to -180"));

        MvcResult result = getRequestResult(-200, VALID_LATITUDE, VALID_RADIUS, status().isBadRequest());

        assertEquals(expectedError, result.getResponse().getContentAsString());
    }

    @Test
    public void testAggregatedSearchGet_invalidLatitude_returns400() throws Exception {
        String expectedError = objectMapper.writeValueAsString(Collections.singletonMap("error",
                "aggregatedSearchGet.latitude: must be less than or equal to 90"));

        MvcResult result = getRequestResult(VALID_LONGITUDE, 100, VALID_RADIUS, status().isBadRequest());

        assertEquals(expectedError, result.getResponse().getContentAsString());
    }

    @Test
    public void testAggregatedSearchGet_invalidRadius_returns400() throws Exception {
        String expectedError = objectMapper.writeValueAsString(Collections.singletonMap("error",
                "aggregatedSearchGet.radius: must be greater than or equal to 1"));

        MvcResult result = getRequestResult(VALID_LONGITUDE, VALID_LATITUDE, -1, status().isBadRequest());

        assertEquals(expectedError, result.getResponse().getContentAsString());
    }

    @Test
    public void testAggregatedSearchGet_missingParam_returns400() throws Exception {
        MvcResult result = mockMvc.perform(get(API_PREFIX + "/aggregated-search")
                .contentType("application/json")
                .param("latitude", String.valueOf(VALID_LATITUDE))
                .param("radius", String.valueOf(VALID_RADIUS))
                .content(""))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Required Double parameter 'longitude' is not present", result.getResponse().getErrorMessage());
    }

    private MvcResult getRequestResult(double longitude, double latitude, double radius,
                                              ResultMatcher expectedStatus) throws Exception {
        return mockMvc.perform(get(API_PREFIX + "/aggregated-search")
                .contentType("application/json")
                .param("longitude", String.valueOf(longitude))
                .param("latitude", String.valueOf(latitude))
                .param("radius", String.valueOf(radius))
                .content(""))
                .andExpect(expectedStatus)
                .andReturn();
    }
}
