package com.holidu.assignment.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TreeDataApiConfig.class})
public class TreeDataApiConfigTest {

    @Autowired
    private TreeDataApiConfig treeDataApiConfig;

    @Test
    public void testGetApi_hasDefaultValue() {
        assertNotNull(treeDataApiConfig.getApi());
    }
}
