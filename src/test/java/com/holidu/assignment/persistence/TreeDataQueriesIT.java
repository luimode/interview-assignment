package com.holidu.assignment.persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.holidu.assignment.persistence.TreeDataPersistence.EXTERNAL_ID;
import static org.junit.Assert.*;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@RunWith(SpringRunner.class)
@DataMongoTest
@Import(TreeDataQueries.class)
public class TreeDataQueriesIT {

    private static final long EXTERNAL_ID_VAL = 1L;
    private static final String SPECIES = "species";
    private static final double LONGITUDE = 50.0;
    private static final double LATITUDE = 60.0;
    private static final GeoJsonPoint HOLIDU_LOCATION = new GeoJsonPoint(11.53784, 48.17882);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private TreeDataQueries treeDataQueries;

    @Before
    public void setup() {
        cleanupDb();
    }

    @After
    public void tearDown() {
        cleanupDb();
    }

    @Test
    public void testBulkInsertTreeData_correctlyStoresData() {
        Long extId2 = 2L;
        String species2 = "species2";
        double longitude2 = 51.0;
        double latitude2 = 61.0;
        TreeDataPersistence treeDataPersistence1 = new TreeDataPersistence(null, EXTERNAL_ID_VAL, SPECIES, LONGITUDE, LATITUDE);
        TreeDataPersistence treeDataPersistence2 = new TreeDataPersistence(null, extId2, species2, longitude2, latitude2);
        treeDataQueries.bulkInsertTreeData(Arrays.asList(treeDataPersistence1, treeDataPersistence2));

        List<TreeDataPersistence> allPersisted = mongoTemplate.findAll(TreeDataPersistence.class);
        assertEquals(2, allPersisted.size());
        TreeDataPersistence stored1 = findByExtId(EXTERNAL_ID_VAL);
        TreeDataPersistence stored2 = findByExtId(extId2);

        assertTreeDataPersistence(stored1, EXTERNAL_ID_VAL, LONGITUDE, LATITUDE, SPECIES);
        assertTreeDataPersistence(stored2, extId2, longitude2, latitude2, species2);
    }

    @Test
    public void testBulkInsertTreeData_existingData_updatesExistingData() {
        String speciesUpdate = "speciesUpdate";
        double longitudeUpdate = 50.0;
        double latitudeUpdate = 60.0;
        TreeDataPersistence existing = new TreeDataPersistence(null, EXTERNAL_ID_VAL, SPECIES, LONGITUDE, LATITUDE);
        TreeDataPersistence updated = new TreeDataPersistence(null, EXTERNAL_ID_VAL, speciesUpdate, longitudeUpdate, latitudeUpdate);
        mongoTemplate.insert(existing);
        // assert data has been correctly stored
        assertNotNull(findByExtId(EXTERNAL_ID_VAL));

        treeDataQueries.bulkInsertTreeData(Collections.singletonList(updated));

        TreeDataPersistence storedUpdate = findByExtId(EXTERNAL_ID_VAL);
        assertTreeDataPersistence(storedUpdate, EXTERNAL_ID_VAL, longitudeUpdate, latitudeUpdate, speciesUpdate);
    }

    @Test
    public void testQueryTreeSpeciesOccurrenceWithinRadius_correctlyFindsLocationsWithinRadius() {
        GeoJsonPoint bahnhofLoc = new GeoJsonPoint(11.53911, 48.17915); // Gaister Bahnhof, dist ~ 150m from Holidu
        GeoJsonPoint hochschuleLoc = new GeoJsonPoint(11.54458, 48.17965 ); // zentraler Hochschulsport, dist ~ 500m
        GeoJsonPoint ringLoc = new GeoJsonPoint(11.52913, 48.17637); // Georg-Brauchle Ring, dist ~ 700m

        TreeDataPersistence bahnhof = new TreeDataPersistence(
                null, 1L, "gaisterBahnhof", bahnhofLoc.getX(), bahnhofLoc.getY());
        TreeDataPersistence hochschule = new TreeDataPersistence(
                null, 2L, "zentralerHochschulsport", hochschuleLoc.getX(), hochschuleLoc.getY());
        TreeDataPersistence gBRIng = new TreeDataPersistence(
                null, 3L, "Georg-BrauchleRing", ringLoc.getX(), ringLoc.getY());
        mongoTemplate.insertAll(Arrays.asList(bahnhof, hochschule, gBRIng));

        assertEquals(3, mongoTemplate.findAll(TreeDataPersistence.class).size());

        List<TreeOccurrenceAggregation> foundBahnhof = findNearHoliduWithRadius(102);
        List<TreeOccurrenceAggregation> foundBahnhofAndSchule = findNearHoliduWithRadius(510);
        List<TreeOccurrenceAggregation> foundAll = findNearHoliduWithRadius(710);

        assertEquals(1, foundBahnhof.size());
        assertEquals("gaisterBahnhof", foundBahnhof.get(0).getSpeciesName());
        assertEquals(2, foundBahnhofAndSchule.size());
        assertEquals("zentralerHochschulsport", foundBahnhofAndSchule.get(0).getSpeciesName());
        assertEquals("gaisterBahnhof", foundBahnhofAndSchule.get(1).getSpeciesName());
        assertEquals(3, foundAll.size());
    }

    private void assertTreeDataPersistence(TreeDataPersistence treeDataPersistence, Long extId, double longitude,
                                           double latitude, String speciesName) {
        assertEquals(extId, treeDataPersistence.getExternalId());
        assertEquals(longitude, treeDataPersistence.getLocation().getX(), 0.01);
        assertEquals(latitude, treeDataPersistence.getLocation().getY(), 0.01);
        assertEquals(speciesName, treeDataPersistence.getCommonSpeciesName());
    }

    private TreeDataPersistence findByExtId(long extId) {
        List<TreeDataPersistence> treeDataPersistences = mongoTemplate.find(query(where(EXTERNAL_ID).is(extId)), TreeDataPersistence.class);
        if (treeDataPersistences.size() != 1) {
            fail("Unexpected number of results, expected 1, found:" + treeDataPersistences.size());
        }
        return treeDataPersistences.get(0);
    }

    private List<TreeOccurrenceAggregation> findNearHoliduWithRadius(double distance) {
        return treeDataQueries.queryTreeSpeciesOccurrenceWithinRadius(HOLIDU_LOCATION.getX(), HOLIDU_LOCATION.getY(), distance);
    }

    private void cleanupDb() {
        mongoTemplate.remove(query(where("id").exists(true)), TreeDataPersistence.class);
    }
}
