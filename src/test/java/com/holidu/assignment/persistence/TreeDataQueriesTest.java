package com.holidu.assignment.persistence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;

import java.util.Collections;
import java.util.List;

import static com.holidu.assignment.persistence.TreeDataPersistence.*;
import static com.holidu.assignment.persistence.TreeOccurrenceAggregation.AGGREGATION_COMMON_SPECIES_NAME;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.data.mongodb.core.query.Query.query;

import com.mongodb.bulk.BulkWriteResult;


@RunWith(MockitoJUnitRunner.class)
public class TreeDataQueriesTest {

    private static final double LONGITUDE = 70;
    private static final double LATITUDE = 70;

    @Mock
    private MongoTemplate mongoTemplate;

    @InjectMocks
    private TreeDataQueries treeDataQueries;

    @Test
    public void testBulkInsertTreeData_callsBulkOpsOnTemplate() {
        String id = "id1";
        Long extId = 1L;
        String species = "species";
        TreeDataPersistence treeDataPersistence = new TreeDataPersistence(id, extId, species, LONGITUDE, LATITUDE);
        List<TreeDataPersistence> treeDataPersistences = Collections.singletonList(treeDataPersistence);

        BulkOperations bulkOpsMock = mock(BulkOperations.class);
        BulkWriteResult bulkWriteResultMock = mock(BulkWriteResult.class, Mockito.RETURNS_DEEP_STUBS);
        when(mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, TreeDataPersistence.class)).thenReturn(bulkOpsMock);
        when(bulkOpsMock.upsert((any()))).thenReturn(bulkOpsMock);
        when(bulkOpsMock.execute()).thenReturn(bulkWriteResultMock);
        when(bulkWriteResultMock.getUpserts().size()).thenReturn(1);

        treeDataQueries.bulkInsertTreeData(treeDataPersistences);

        verify(mongoTemplate).bulkOps(BulkOperations.BulkMode.UNORDERED, TreeDataPersistence.class);
        verify(bulkOpsMock).upsert(
                Collections.singletonList(Pair.of(
                        query(Criteria.where(EXTERNAL_ID).is(extId)),
                        new Update()
                                .set(EXTERNAL_ID, extId)
                                .set(PERSISTENCE_COMMON_SPECIES_NAME, species)
                                .set(LOCATION, new GeoJsonPoint(LONGITUDE, LATITUDE))
                )));
        verify(bulkOpsMock).execute();
    }

    @Test
    public void queryTreeSpeciesOccurrenceWithinRadius_buildsAggregationCorrectly() {
        double maxDistanceMeters = 100;
        String distanceField = "distance";

        AggregationResults aggregationResultsMock = mock(AggregationResults.class);
        when(mongoTemplate.aggregate(any(Aggregation.class), eq(TreeDataPersistence.class),
                eq(TreeOccurrenceAggregation.class))).thenReturn(aggregationResultsMock);
        when(aggregationResultsMock.getMappedResults()).thenReturn(Collections.emptyList());
        ArgumentCaptor<Aggregation> aggregationCaptor = ArgumentCaptor.forClass(Aggregation.class);

        treeDataQueries.queryTreeSpeciesOccurrenceWithinRadius(LONGITUDE, LATITUDE, maxDistanceMeters);

        verify(mongoTemplate).aggregate(aggregationCaptor.capture(), eq(TreeDataPersistence.class),
                eq(TreeOccurrenceAggregation.class));
        Aggregation capturedAggregation = aggregationCaptor.getAllValues().get(0);

        // see https://docs.mongodb.com/manual/tutorial/calculate-distances-using-spherical-geometry-with-2d-geospatial-indexes/
        double expectedMaxDistanceWithCoef = (maxDistanceMeters / 1000) / 6378.137;

        String expectedJson = "{ \"aggregate\" : \"__collection__\", \"pipeline\" : [{ \"$geoNear\" : " +
                "{ \"maxDistance\" : " + expectedMaxDistanceWithCoef + ", \"distanceMultiplier\" : 6378.137, " +
                "\"near\" : [" + LONGITUDE + ", " + LATITUDE + "], \"spherical\" : true, " +
                "\"distanceField\" : \"" + distanceField + "\" } }," +
                " { \"$group\" : { \"_id\" : \"$" + PERSISTENCE_COMMON_SPECIES_NAME + "\", \"count\" : { \"$sum\" : 1 } } }," +
                " { \"$project\" : { \"count\" : 1, \"" + AGGREGATION_COMMON_SPECIES_NAME + "\" : \"$_id\" } }] }";

        assertEquals(expectedJson, capturedAggregation.toString());
    }
}
