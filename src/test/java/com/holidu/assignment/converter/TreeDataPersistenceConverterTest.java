package com.holidu.assignment.converter;

import com.holidu.assignment.TestUtils;
import com.holidu.assignment.fetch.ExternalTreeData;
import com.holidu.assignment.persistence.TreeDataPersistence;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class TreeDataPersistenceConverterTest {

    private static final String SPECIES = "species";
    private static final Long ID = 1L;
    private static final Double LONGITUDE = 110.0;
    private static final Double LATITUDE = 60.0;

    @Test
    public void testDecodeExternalTreeDataList_valid_convertsToTreeDataPersistenceList() {
        String species2 = "species2";
        Long id2 = 2L;
        Double longitude2 = 100.0;
        Double latitude2 = 60.0;

        List<ExternalTreeData> externalTreeData = Arrays.asList(
                TestUtils.createExternalTreeData(SPECIES, ID, LONGITUDE, LATITUDE),
                TestUtils.createExternalTreeData(species2, id2, longitude2, latitude2));

        List<TreeDataPersistence> treeDataPersistences =
                TreeDataPersistenceConverter.decodeExternalTreeDataList(externalTreeData);

        assertEquals(2, treeDataPersistences.size());
        assertTreeDataPersistence(treeDataPersistences.get(0), SPECIES, ID, LONGITUDE, LATITUDE);
        assertTreeDataPersistence(treeDataPersistences.get(1), species2, id2, longitude2, latitude2);
    }

    @Test
    public void testDecodeExternalTreeDataList_nullList_returnsEmptyList() {
        List<TreeDataPersistence> treeDataPersistences = TreeDataPersistenceConverter.decodeExternalTreeDataList(null);
        assertNotNull(treeDataPersistences);
        assertTrue(treeDataPersistences.isEmpty());
    }

    @Test
    public void testDecodeExternalTreeDataList_emptyList_returnsEmptyList() {
        List<TreeDataPersistence> treeDataPersistences =
                TreeDataPersistenceConverter.decodeExternalTreeDataList(Collections.emptyList());
        assertNotNull(treeDataPersistences);
        assertTrue(treeDataPersistences.isEmpty());
    }

    @Test
    public void testDecodeExternalTreeData_valid_convertsToTreeDataPersistence() {
        ExternalTreeData externalTreeData = TestUtils.createExternalTreeData(SPECIES, ID, LONGITUDE, LATITUDE);
        TreeDataPersistence treeDataPersistence = TreeDataPersistenceConverter.decodeExternalTreeData(externalTreeData);
        assertTreeDataPersistence(treeDataPersistence, SPECIES, ID, LONGITUDE, LATITUDE);
    }

    @Test
    public void testDecodeExternalTreeData_null_returnsNull() {
        assertNull(TreeDataPersistenceConverter.decodeExternalTreeData(null));
    }

    private void assertTreeDataPersistence(TreeDataPersistence persistence, String species, Long id, Double longitude,
                                           Double latitude ) {
        assertEquals(id, persistence.getExternalId());
        assertEquals(species, persistence.getCommonSpeciesName());
        assertEquals(longitude, persistence.getLocation().getCoordinates().get(0));
        assertEquals(latitude, persistence.getLocation().getCoordinates().get(1));
    }
}
