package com.holidu.assignment;

import com.holidu.assignment.fetch.ExternalTreeData;

public class TestUtils {

    private TestUtils() { }

    public static ExternalTreeData createExternalTreeData(String speciesName, long extId, double longitude, double latitude) {
        ExternalTreeData externalTreeData = new ExternalTreeData();
        externalTreeData.setCommonSpeciesName(speciesName);
        externalTreeData.setId(extId);
        externalTreeData.setLongitude(longitude);
        externalTreeData.setLatitude(latitude);
        return externalTreeData;
    }
}
