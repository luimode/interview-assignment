package com.holidu.assignment.fetch;

import com.holidu.assignment.TestUtils;
import com.holidu.assignment.converter.TreeDataPersistenceConverter;
import com.holidu.assignment.persistence.TreeDataQueries;
import feign.FeignException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ExternalTreeDataLoaderTest {

    @Mock
    ExternalTreeDataApi externalTreeDataApi;

    @Mock
    TreeDataQueries treeDataQueries;

    @InjectMocks
    ExternalTreeDataLoader externalTreeDataLoader;

    @Test
    public void testLoadTreeData_fetchSuccess_callsBulkInsertQuery()throws IOException {
        List<ExternalTreeData> fetchedTreeData = Arrays.asList(
            TestUtils.createExternalTreeData("species", 1L, 1.0, 2.0));

        when(externalTreeDataApi.getTreeData()).thenReturn(fetchedTreeData);

        externalTreeDataLoader.loadTreeData();

        verify(externalTreeDataApi).getTreeData();
        verify(treeDataQueries).bulkInsertTreeData(
                TreeDataPersistenceConverter.decodeExternalTreeDataList(fetchedTreeData));
    }

    @Test
    public void testLoadTreeData_fetchFails_rethrowsIOException(){
        when(externalTreeDataApi.getTreeData()).thenThrow(FeignException.class);

        try {
            externalTreeDataLoader.loadTreeData();
            fail("IOException expected");
        } catch (IOException e) {
            // expected
        }

        verify(externalTreeDataApi).getTreeData();
        verify(treeDataQueries, never()).bulkInsertTreeData(any());
    }

    @Test
    public void testLoadTreeData_fetchReturnsNull_safeNOP()throws IOException {
        when(externalTreeDataApi.getTreeData()).thenReturn(null);

        externalTreeDataLoader.loadTreeData();

        verify(externalTreeDataApi).getTreeData();
        verifyZeroInteractions(treeDataQueries);
    }

}
