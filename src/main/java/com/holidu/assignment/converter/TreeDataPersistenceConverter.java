package com.holidu.assignment.converter;

import com.holidu.assignment.fetch.ExternalTreeData;
import com.holidu.assignment.persistence.TreeDataPersistence;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TreeDataPersistenceConverter {

    private TreeDataPersistenceConverter(){}

    public static List<TreeDataPersistence> decodeExternalTreeDataList(List<ExternalTreeData> treeDatalist) {
        if (treeDatalist == null) {
            return Collections.emptyList();
        }

        return treeDatalist
                .stream()
                .map(TreeDataPersistenceConverter::decodeExternalTreeData)
                .collect(Collectors.toList());
    }

    public static TreeDataPersistence decodeExternalTreeData(ExternalTreeData treeData) {
        if (treeData == null) {
            return null;
        }
        TreeDataPersistence treeDataPersistence = new TreeDataPersistence();
        treeDataPersistence.setExternalId(treeData.getId());
        treeDataPersistence.setCommonSpeciesName(treeData.getCommonSpeciesName());
        treeDataPersistence.setLocation(new GeoJsonPoint(treeData.getLongitude(), treeData.getLatitude()));

        return treeDataPersistence;
    }
}
