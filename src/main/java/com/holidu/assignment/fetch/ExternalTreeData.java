package com.holidu.assignment.fetch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalTreeData
{

    @JsonProperty("tree_id")
    private Long id;

    @JsonProperty("spc_common")
    private String commonSpeciesName;

    private double latitude;

    private double longitude;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCommonSpeciesName()
    {
        return commonSpeciesName;
    }

    public void setCommonSpeciesName(String commonSpeciesName)
    {
        this.commonSpeciesName = commonSpeciesName;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    @Override
    public String toString()
    {
        return "CityTree{" +
            "id=" + id +
            ", commonSpeciesName='" + commonSpeciesName + '\'' +
            ", latitude=" + latitude +
            ", longitude=" + longitude +
            '}';
    }
}
