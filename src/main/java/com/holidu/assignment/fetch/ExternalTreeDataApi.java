package com.holidu.assignment.fetch;

import feign.RequestLine;

import java.util.List;

public interface ExternalTreeDataApi {

    @RequestLine("GET /resource/nwxe-4ae8.json")
    List<ExternalTreeData> getTreeData();

}
