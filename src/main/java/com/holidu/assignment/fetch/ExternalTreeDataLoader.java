package com.holidu.assignment.fetch;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.holidu.assignment.converter.TreeDataPersistenceConverter;
import com.holidu.assignment.persistence.TreeDataPersistence;
import com.holidu.assignment.persistence.TreeDataQueries;

import feign.FeignException;


@Service
public class ExternalTreeDataLoader {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ExternalTreeDataApi externalTreeDataApi;

    private final TreeDataQueries queries;

    public ExternalTreeDataLoader(ExternalTreeDataApi externalTreeDataApi, TreeDataQueries treeDataQueries) {
        this.externalTreeDataApi = externalTreeDataApi;
        this.queries = treeDataQueries;
    }

    public void loadTreeData() throws IOException {
        try {
            List<ExternalTreeData> data = externalTreeDataApi.getTreeData();
            if (data == null) {
                return;
            }
            removeRecordsWithEmptySpecName(data);
            logger.info("Tree data successfully fetched, size:{}", data != null ? data.size() : 0);
            saveTreeData(data);
        } catch (FeignException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    private void removeRecordsWithEmptySpecName(List<ExternalTreeData> data) {
        data.removeIf(treeDataItem ->
            treeDataItem.getCommonSpeciesName() == null || treeDataItem.getCommonSpeciesName().isEmpty());
    }

    private void saveTreeData(List<ExternalTreeData> externalTreeData) {
        List<TreeDataPersistence> treeDataPersistences =
                TreeDataPersistenceConverter.decodeExternalTreeDataList(externalTreeData);
        queries.bulkInsertTreeData(treeDataPersistences);
    }
}
