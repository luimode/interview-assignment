package com.holidu.assignment.config;

import static org.springframework.util.Assert.hasText;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.holidu.assignment.fetch.ExternalTreeDataApi;

import feign.Feign;
import feign.jackson.JacksonDecoder;

@Configuration
public class TreeDataApiConfig {

    @Value("${tree-data-api.url}")
    private String treeDataApiUrl;

    @Bean(name = "TreeDataApi")
    public ExternalTreeDataApi getApi() {
        hasText(treeDataApiUrl, "treeDataApiUrl should not be null!");

        Feign.Builder builder = Feign.builder().decoder(new JacksonDecoder());
        return builder.target(ExternalTreeDataApi.class, treeDataApiUrl);
    }
}
