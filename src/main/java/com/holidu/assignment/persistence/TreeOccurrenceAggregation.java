package com.holidu.assignment.persistence;

import org.springframework.data.mongodb.core.mapping.Field;

public class TreeOccurrenceAggregation {

    public static final String COUNT_FIELD = "count";
    public static final String AGGREGATION_COMMON_SPECIES_NAME = "speciesName";

    @Field(AGGREGATION_COMMON_SPECIES_NAME)
    private String speciesName;

    @Field(COUNT_FIELD)
    private int count;

    public TreeOccurrenceAggregation() { }

    public TreeOccurrenceAggregation(String speciesName, int count) {
        this.speciesName = speciesName;
        this.count = count;
    }

    public String getSpeciesName() {
        return speciesName;
    }

    public void setSpeciesName(String speciesName) {
        this.speciesName = speciesName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "TreeDataAggregation{" +
                "speciesName='" + speciesName + '\'' +
                ", count=" + count +
                '}';
    }
}
