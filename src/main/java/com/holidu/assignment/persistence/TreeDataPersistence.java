package com.holidu.assignment.persistence;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Objects;

@Document(collection = "TreeData")
public class TreeDataPersistence {

    public static final String PERSISTENCE_COMMON_SPECIES_NAME = "commonSpeciesName";
    public static final String EXTERNAL_ID = "externalId";
    public static final String LOCATION = "location";

    @Id
    private String id;

    @Field(EXTERNAL_ID)
    private Long externalId;

    @Field(PERSISTENCE_COMMON_SPECIES_NAME)
    private String commonSpeciesName;

    @GeoSpatialIndexed (type = GeoSpatialIndexType.GEO_2DSPHERE)
    @Field(LOCATION)
    private GeoJsonPoint location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getExternalId() {
        return externalId;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }

    public TreeDataPersistence() { }

    public TreeDataPersistence(String id, Long extId, String species, double longitude, double latitude) {
        this.id = id;
        this.externalId = extId;
        this.commonSpeciesName = species;
        this.location = new GeoJsonPoint(longitude, latitude);
    }

    public String getCommonSpeciesName() {
        return commonSpeciesName;
    }

    public void setCommonSpeciesName(String commonSpeciesName) {
        this.commonSpeciesName = commonSpeciesName;
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPoint location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "TreeDataPersistence{" +
                "id='" + id + '\'' +
                ", externalId=" + externalId +
                ", commonSpeciesName='" + commonSpeciesName + '\'' +
                ", location=" + location +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreeDataPersistence that = (TreeDataPersistence) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getExternalId(), that.getExternalId()) &&
                Objects.equals(getCommonSpeciesName(), that.getCommonSpeciesName()) &&
                Objects.equals(getLocation(), that.getLocation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getExternalId(), getCommonSpeciesName(), getLocation());
    }
}
