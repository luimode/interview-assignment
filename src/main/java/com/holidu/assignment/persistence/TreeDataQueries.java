package com.holidu.assignment.persistence;

import com.mongodb.bulk.BulkWriteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.geo.Metrics;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.holidu.assignment.persistence.TreeDataPersistence.*;
import static com.holidu.assignment.persistence.TreeOccurrenceAggregation.AGGREGATION_COMMON_SPECIES_NAME;
import static com.holidu.assignment.persistence.TreeOccurrenceAggregation.COUNT_FIELD;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class TreeDataQueries {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final MongoTemplate template;

    public TreeDataQueries(MongoTemplate mongoTemplate) {
        this.template = mongoTemplate;
    }

    public void bulkInsertTreeData(List<TreeDataPersistence> treeDataPersistences){
        BulkOperations bulkOps = template.bulkOps(BulkOperations.BulkMode.UNORDERED, TreeDataPersistence.class);

        BulkOperations upserts = bulkOps.upsert(toUpsertPairs(treeDataPersistences));

        BulkWriteResult result = upserts.execute();
        logger.debug("Successfully inserted {} documents", result.getMatchedCount());
    }

    private List<Pair<Query, Update>> toUpsertPairs(List<TreeDataPersistence> treeDataPersistences) {
        return treeDataPersistences.stream()
                .map(treeDataPersistence ->
                        Pair.of(
                                query(where(EXTERNAL_ID).is(treeDataPersistence.getExternalId())),
                                new Update()
                                        .set(EXTERNAL_ID, treeDataPersistence.getExternalId())
                                        .set(PERSISTENCE_COMMON_SPECIES_NAME, treeDataPersistence.getCommonSpeciesName())
                                        .set(LOCATION, treeDataPersistence.getLocation()))
                ).collect(Collectors.toList());
    }

    public List<TreeOccurrenceAggregation> queryTreeSpeciesOccurrenceWithinRadius(double longitude, double latitude,
                                                                                  double metersDistance) {
        double kmDistance = metersDistance / 1000;
        List<AggregationOperation> aggregationOps = new ArrayList<>();
        NearQuery nearQuery = NearQuery
                .near(longitude, latitude)
                .maxDistance(kmDistance, Metrics.KILOMETERS)
                .spherical(true);
        aggregationOps.add(Aggregation.geoNear(nearQuery, "distance"));
        aggregationOps.add(Aggregation.group(PERSISTENCE_COMMON_SPECIES_NAME).count().as(COUNT_FIELD));
        aggregationOps.add(Aggregation.project(COUNT_FIELD).andExpression("_id").as(AGGREGATION_COMMON_SPECIES_NAME));
        Aggregation aggregation = Aggregation.newAggregation(aggregationOps);

        return template.aggregate(aggregation, TreeDataPersistence.class, TreeOccurrenceAggregation.class).getMappedResults();
    }

}
