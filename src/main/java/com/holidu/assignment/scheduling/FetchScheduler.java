package com.holidu.assignment.scheduling;

import com.holidu.assignment.fetch.ExternalTreeDataLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

@EnableScheduling
@Service
public class FetchScheduler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ExternalTreeDataLoader externalTreeDataLoader;

    public FetchScheduler(ExternalTreeDataLoader externalTreeDataLoader) {
        this.externalTreeDataLoader = externalTreeDataLoader;
    }

    /**
     * In our artificial case, a one-time data fetch is enough. However, if this was a "real" app,
     * the data behind the api would likely change over time and a scheduled periodical fetch
     * would be required to update the existing data.
     */
    @Scheduled(fixedDelayString = "${job.fetch.fixed-delay}", initialDelay = 3000)
    public void fetchTreeData() {
        try {
            externalTreeDataLoader.loadTreeData();
        } catch (IOException e) {
            logger.warn("Error while loading data:{}, {}", e.getMessage(), e);
        }
    }
}
