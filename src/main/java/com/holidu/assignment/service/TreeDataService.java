package com.holidu.assignment.service;

import com.holidu.assignment.api.OccurrenceAggregation;
import com.holidu.assignment.persistence.TreeOccurrenceAggregation;
import com.holidu.assignment.persistence.TreeDataQueries;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TreeDataService {

    private final TreeDataQueries treeDataQueries;

    public TreeDataService(TreeDataQueries treeDataQueries) {
        this.treeDataQueries = treeDataQueries;
    }

    public OccurrenceAggregation getTreeSpeciesWithinRadius(double longitude, double latitude, double radius) {
        List<TreeOccurrenceAggregation> treeOccurrenceAggregations =
                treeDataQueries.queryTreeSpeciesOccurrenceWithinRadius(longitude, latitude, radius);
        return toSearchResult(treeOccurrenceAggregations);
    }

    private OccurrenceAggregation toSearchResult(List<TreeOccurrenceAggregation> occurrenceAggregations) {
        OccurrenceAggregation aggregatedSearchResult = new OccurrenceAggregation();
        if (occurrenceAggregations == null) {
            return aggregatedSearchResult;
        }
        occurrenceAggregations
                .forEach(aggregation ->
                        aggregatedSearchResult.put(aggregation.getSpeciesName(), aggregation.getCount()));
        return aggregatedSearchResult;
    }
}
