package com.holidu.assignment.rest;

import com.holidu.assignment.api.AggregatedSearchApi;
import com.holidu.assignment.api.OccurrenceAggregation;
import com.holidu.assignment.service.TreeDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@Validated
public class TreeDataRestController implements AggregatedSearchApi {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final TreeDataService treeDataService;

    public TreeDataRestController(TreeDataService treeDataService) {
        this.treeDataService = treeDataService;
    }

    @Override
    public ResponseEntity<OccurrenceAggregation> aggregatedSearchGet(
            @RequestParam(value = "longitude") Double longitude,
            @RequestParam(value = "latitude") Double latitude,
            @RequestParam(value = "radius") Double radius) {

        logger.info("Requested aggregated search with longitude:{} latitude:{} radius:{}", longitude, latitude, radius);

        OccurrenceAggregation treeSpeciesWithinRadius = treeDataService.getTreeSpeciesWithinRadius(longitude, latitude, radius);

        logger.debug("Search success, aggregation result:{}", treeSpeciesWithinRadius);
        logger.info("Search success, returning {} results, status:{}", treeSpeciesWithinRadius.size(), HttpStatus.OK);
        return new ResponseEntity<>(treeSpeciesWithinRadius, HttpStatus.OK);
    }

}
