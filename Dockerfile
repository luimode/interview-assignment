FROM openjdk:8-jdk-alpine

ENV JAVA_OPTS="-Xmx800m -server -XX:+UseConcMarkSweepGC"
RUN mkdir -p /service
WORKDIR /service
COPY target/interview-assignment*.jar /service/interview-assignment.jar
EXPOSE 8080

CMD java ${JAVA_OPTS} -jar /service/interview-assignment.jar
